<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>Kanna Blog</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/highlight.min.css">
<script src="js/highlight.min.js"></script>
<body>

<?php
    $servername = "localhost";
    $username = "root";
    $password = "";

    // Create connection
    $conn = new mysqli($servername, $username, $password);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed");
    }

	$queries = array();
	parse_str($_SERVER['QUERY_STRING'], $queries);

	if(!isset($queries["id"])) {
		die("id must be passed");
	}

	$id = $queries["id"];
	
	if((int)$id <= 0) {
		die("id must be an positive integer!");
	}

    $sql = "SELECT id,title, created_at, authors,content, description FROM blog.post WHERE id=".$id." LIMIT 1";
    $result = $conn->query($sql); ?>

<div class="container center">
	<a class="pink1" href="index.php">kanna blog</a>

	<?php
	$nb_rows = $result->num_rows;
	$count = 0;
	if ($nb_rows > 0) {
		$row = $result->fetch_assoc();
		echo "<div class='blog_header'>"
		."<h1 class='light_blue'>". $row["title"] ."</h1>"
		."<span class='violet italic'>". $row["authors"] . " " . "<span class='pink2 italic'>". $row["created_at"] ."</span> </span>"
		."</div>"
		."<div class='content'><p>". $row["content"] ."</p></div>";
	} else {
		echo "blog post not found!!";
	}

	$conn->close();
?>

<a class="pink1 moreA" href="all.php">Blog Index</a>


<script>
	hljs.highlightAll();
</script>

</div>


</body>
</html>
