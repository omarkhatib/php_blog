document.getElementById("mdbox").focus();
showdown.setFlavor('github')
var converter = new showdown.Converter()
converter.setOption('tables', true)
converter.setOption('tasklists',true)
converter.setOption('simpleLineBreaks',true)
converter.setOption('smoothLivePreview',true)

// NOTE(omarkhatib): this is for future editing for a post
// the html will be saved in database , so as reverse step
// the saved HTML will be converted back to html
html = ""
if (html != "") {
	document.getElementById("mdbox").value = converter.makeMarkdown(html)
}

const convert = () => {
    text = document.getElementById("mdbox").value
	if (text == "") {
		html = "live view will be displayed here"
	} else {
		html = converter.makeHtml(text)
	}

    document.getElementById("preview").innerHTML = html
    document.getElementById("html_content").innerHTML = html
	hljs.highlightAll();
    return html
}

// Thanks stack over flow
const insertTab = (o, e) => {
    var kC = e.keyCode ? e.keyCode : e.charCode ? e.charCode : e.which
    if (kC == 9 && !e.shiftKey && !e.ctrlKey && !e.altKey) {
        var oS = o.scrollTop
        if (o.setSelectionRange) {
            var sS = o.selectionStart
            var sE = o.selectionEnd
            o.value = o.value.substring(0, sS) + "\t" + o.value.substr(sE)
            o.setSelectionRange(sS + 1, sS + 1)
            o.focus()
        } else if (o.createTextRange) {
            document.selection.createRange().text = "\t"
            e.returnValue = false
        }
        o.scrollTop = oS
        if (e.preventDefault)
            e.preventDefault()
        return false
    }
    return true
}

const uploadImage = (e) => {
    var uploadBtn=document.getElementById("upload_image");
    uploadBtn.onsubmit=function(e)
    {
        e.preventDefault();
    }
}

const initFromCache = () => {
    let title = localStorage['title'] || '';
    let authors = localStorage['authors'] || '';
    let description = localStorage['description'] || '';
    let md = localStorage['md'] || '';

    document.getElementById("title").value = title != null ? title : '';
    document.getElementById("authors").value = authors != null ? authors : '';
    document.getElementById("description").value = description != null ? description : '';
    document.getElementById("mdbox").value = md != null ? md : '';
    convert()
}

const updateCache = () => {
    let title = document.getElementById("title").value
    let authors = document.getElementById("authors").value
    let description = document.getElementById("description").value
    let md = document.getElementById("mdbox").value

    localStorage.setItem('title' , title)
    localStorage.setItem('authors' , authors)
    localStorage.setItem('description' , description)
    localStorage.setItem('md' , md)
}

// https://dev.to/eaich/how-to-detect-when-the-user-stops-typing-3cm1
let timer,timeoutVal = 500;

const mdbox = document.getElementById('mdbox')
const inputs = document.getElementsByTagName('input')

const doEvent = (element , ...f) => {
    element.addEventListener('keypress', handleKeyPress)
    element.addEventListener('keyup', handleKeyUp)

    function handleKeyPress(e) {
        window.clearTimeout(timer)
    }

    function handleKeyUp(e) {
        window.clearTimeout(timer);
        for(let i=0 ; i < inputs.length; i++) {
            timer = window.setTimeout(() => f[i](), timeoutVal)
        }
    }
}

doEvent(mdbox , convert , updateCache)

for(let i=0 ; i < inputs.length; i++) {
    doEvent(inputs[i] , updateCache)
}