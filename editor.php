<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>Kanna Blog</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/highlight.min.css">
<script src="js/highlight.min.js"></script>

<body style="overflow: hidden;">

<style>
body {
      margin: 0;
      min-height: 100vh;
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-template-rows: auto 1fr;
}

body {
	scrollbar-color: #beb9c8 #efeef800;
}
</style>

	<header>
		<form action="editor.php" method="POST">
		<div class="header__left">
			<input name="title" id="title" style="width:20%;" type="text" placeholder="title here">
			<input name="authors" id="authors" style="width:20%;" type="text" placeholder="authors here">
			<input name="description" id="description" style="width:50%;" type="text" placeholder="short description here">
			<textarea name="html_content" id="html_content"></textarea>
		</div>
	
		<div class="header__right">
			<button class="editor_button" name="upload_image" type="button" onclick="alert('to be implemented')">upload image</button>
			<button class="editor_button" name="publish" id="publish" onclick="convert()" >publish</button>
		</div>

		</form>

	</header>

	<div class="left-half">
		<textarea class="scrollbar" onkeydown="insertTab(this, event)" id="mdbox" name="mdbox" placeholder="Write md here" ></textarea>
	</div>
	<div class="right-half">
		<div class="scrollbar" id="preview" name="preview"></div>
	</div>


	<img class="kanna_corner" src="images/kanna_corner.png"/>

	
	<script src="js/showdown.min.js"> </script>
	<script src="js/editor.js"></script>
	<script>initFromCache()</script>
</body>

</html>

<?php
	if(isset($_POST["publish"]))
		if(isset($_POST["title"]) && isset($_POST["authors"]) && isset($_POST["description"]) && isset($_POST["html_content"]) ) {
			if ($_POST["title"] == "" || $_POST["authors"]=="" || $_POST["description"] == "" || $_POST["html_content"] == "") {
				echo "<script>alert('please enter all inputs')</script>";
			} else {
				if($_POST["html_content"] == "live view will be displayed here") {
					echo "Error";
				} else {
					$servername = "localhost";
					$username = "root";
					$password = "";

					// Create connection
					$db = new mysqli($servername, $username, $password);

					// Check connection
					if ($db->connect_error)
						die("Connection failed");
    				
					$sql = "INSERT INTO blog.post (title, authors, description , content) VALUES (?,?,?,?)";
					$stmt = $db->prepare($sql);
					$stmt->bind_param("ssss",$_POST["title"],$_POST["authors"],$_POST["description"],$_POST["html_content"]);
					if($stmt->execute()) {
						echo "<script>localStorage.clear()</script>";
						echo "<script>alert('Done!')</script>";
					} else {
						echo "<script>alert('Error Happened!')</script>";
					}
				}
			}
		} else {
			echo "<script>alert('please enter all inputs')</script>";
		}
	unset($_POST);
?>