<?php
    $servername = "localhost";
    $username = "root";
    $password = "";

    // Create connection
    $conn = new mysqli($servername, $username, $password);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed");
    }

    $sql = "SELECT id,title, created_at, authors, description FROM blog.post ORDER BY created_at DESC LIMIT 6";
    $result = $conn->query($sql); ?>

    <!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>Kanna Blog</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<script src=""></script>
<body>

<div class="center">
	<img draggable=false class="logo center" src="images/logo.png"/>
	<h1 class="light_blue center">Kanna Blog</h1>
</div>

<div class="container center">
    <?php
        $nb_rows = $result->num_rows;
        $count = 0;
        if ($nb_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                if ($count == 5) break;
                $count++;
                echo "<p>"
                ."<a href='". "blog.php?id=" . $row["id"] ."' class='pink1'>".  $row["title"] . " </a>"
                ."<span class='pink2'>". $row["created_at"] ."</span><br>"
                ."<span class='violet italic'>". $row["authors"] ."</span><br>"
                . $row["description"]
                ."</p><hr>";
            } 
        } else {
            echo "no posts yet...";
        }
        if($nb_rows > 5) {
            echo "<a href='all.php' class='moreA pink1'>more articles...</a>";
        }
        $conn->close();
    ?>
	
</div>

</body>
</html> 
